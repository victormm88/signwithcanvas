package cd.vmontes.com.mysign.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by vmontes on 20/2/17.
 */

public class PermissionsManager {

    private Activity activity;

    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;


    public PermissionsManager(Activity activity) {
        this.activity = activity;
    }

    public void externalStoragePermission(String permission, int permissionValue){
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, permissionValue);
            }
        }
    }

    public void grantedAcces(int requestCode,String permissions[], int[] grantResults){
        switch (requestCode) {
            case PermissionsManager.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }
}
