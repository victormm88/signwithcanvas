package cd.vmontes.com.mysign.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by VíctorM on 05/02/2017.
 */

public class MyView extends View {
    public int width;
    public  int height;
    Paint mPaint = new Paint();
    float mX, mY;
    Path pathDraw;
    private static final float TOLERANCE = 5;

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint   mBitmapPaint;

    int withStart;
    int withEnd;
    int heightStart;
    int heightEnd;


    public MyView(Context c, AttributeSet attrs) {
        super(c, attrs);
        init();
    }

    public MyView(Context context) {
        super(context);
        init();
    }

    private void init(){
        pathDraw = new Path();

        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(4f);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        TypedValue tv = new TypedValue();

        int actionBarHeight = getActionBarHeight(tv);
        int width           = getActivityWidth();
        int height          = getActivityHeight();

        width = this.getWidth();
        height = this.getHeight();

        //printCells(10, canvas, width, height);
        //canvas.drawRect(left,top,right,bottom,paint);
        withStart   = (width/10);
        withEnd     = (width/10) * 9;
        heightStart = ((height/10) * 3) - actionBarHeight;
        heightEnd   = ((height/10) * 9) - actionBarHeight;

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

        //Area de dibujo
        canvas.clipRect(withStart, heightStart, withEnd, heightEnd, Region.Op.REPLACE);
        //Border
        canvas.drawRect(withStart, heightStart+1, withEnd-1, heightEnd-2, mPaint);

        // draw the mPath with the mPaint on the canvas when onDraw
        canvas.drawPath(pathDraw, mPaint);

    }

    private void printCells(int numberOfCells, Canvas canvas, int width, int height){

        int divPantalla = numberOfCells;

        canvas.drawLine(0, 0, width, 0, mPaint);
        canvas.drawLine(0, (height/divPantalla), width, (height/divPantalla), mPaint);
        canvas.drawLine(0, (height/divPantalla)*2, width, (height/divPantalla)*2, mPaint);
        canvas.drawLine(0, (height/divPantalla)*3, width, (height/divPantalla)*3, mPaint);
        canvas.drawLine(0, (height/divPantalla)*4, width, (height/divPantalla)*4, mPaint);
        canvas.drawLine(0, (height/divPantalla)*5, width, (height/divPantalla)*5, mPaint);
        canvas.drawLine(0, (height/divPantalla)*6, width, (height/divPantalla)*6, mPaint);
        canvas.drawLine(0, (height/divPantalla)*7, width, (height/divPantalla)*7, mPaint);
        canvas.drawLine(0, (height/divPantalla)*8, width, (height/divPantalla)*8, mPaint);
        canvas.drawLine(0, (height/divPantalla)*9, width, (height/divPantalla)*9, mPaint);
        canvas.drawLine(0, (height/divPantalla)*10, width, (height/divPantalla)*10, mPaint);

        canvas.drawLine(0, 0, 0, height, mPaint);
        canvas.drawLine((width/divPantalla), 0, (width/divPantalla), height, mPaint);
        canvas.drawLine((width/divPantalla)*2, 0, (width/divPantalla)*2, height, mPaint);
        canvas.drawLine((width/divPantalla)*3, 0, (width/divPantalla)*3, height, mPaint);
        canvas.drawLine((width/divPantalla)*4, 0, (width/divPantalla)*4, height, mPaint);
        canvas.drawLine((width/divPantalla)*5, 0, (width/divPantalla)*5, height, mPaint);
        canvas.drawLine((width/divPantalla)*6, 0, (width/divPantalla)*6, height, mPaint);
        canvas.drawLine((width/divPantalla)*7, 0, (width/divPantalla)*7, height, mPaint);
        canvas.drawLine((width/divPantalla)*8, 0, (width/divPantalla)*8, height, mPaint);
        canvas.drawLine((width/divPantalla)*9, 0, (width/divPantalla)*9, height, mPaint);
        canvas.drawLine((width/divPantalla)*10, 0, (width/divPantalla)*10, height, mPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    private int getActionBarHeight(TypedValue tv){
        int actionBarHeight = 0;
        if (getContext().getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }

        return actionBarHeight;
    }

    private int getActivityWidth(){
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return  metrics.widthPixels;
    }

    private int getActivityHeight(){
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return  metrics.heightPixels;
    }

    private void startTouch(float x, float y) {
        pathDraw.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            pathDraw.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }


    private void upTouch() {
        pathDraw.lineTo(mX, mY);
    }

    public void clearCanvas() {
        pathDraw.reset();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    public void save()
    {
        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(this.getDrawingCache());
        this.setDrawingCacheEnabled(false);

        //Bitmap bitmap = getDrawingCache();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        File file = new File(path+File.separator+"name"+".png");
        try
        {
            if(!file.exists())

            {
                file.createNewFile();
            }
            FileOutputStream ostream = new FileOutputStream(file);

            Bitmap save = Bitmap.createBitmap(withEnd-withStart, heightEnd-heightStart, Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            Canvas now = new Canvas(save);
            now.drawRect(new Rect(withStart, heightStart, withEnd, heightEnd), paint);
            now.drawBitmap(bmp, new Rect(withStart,heightStart,bmp.getWidth(),bmp.getHeight()), new Rect(withStart, heightStart, withEnd, heightEnd), null);

            save.compress(Bitmap.CompressFormat.PNG, 100, ostream);

            ostream.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
