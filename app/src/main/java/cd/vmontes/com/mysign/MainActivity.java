package cd.vmontes.com.mysign;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import cd.vmontes.com.mysign.permissions.PermissionsManager;

import cd.vmontes.com.mysign.canvas.MyView;

public class MainActivity extends AppCompatActivity {

    MyView myView;
    MyView customCanvas;
    PermissionsManager permissionsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customCanvas = (MyView) findViewById(R.id.signature_canvas);
        //setContentView(new MyView(this));

        Button cleanCanvas = (Button)findViewById(R.id.clean);
        Button saveCanvas   = (Button)findViewById(R.id.save);

        permissionsManager = new PermissionsManager(MainActivity.this);
        permissionsManager.externalStoragePermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionsManager.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);


        cleanCanvas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearCanvas(view);
            }
        });

        saveCanvas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCanvas(view);
            }
        });
    }

    public void clearCanvas(View v) {
        customCanvas.clearCanvas();
    }

    public void saveCanvas(View v){
        customCanvas.save();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        permissionsManager.grantedAcces(requestCode, permissions, grantResults);
    }
}
